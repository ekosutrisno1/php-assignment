<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Biodata;

class BiodataController extends Controller
{
    public function index()
    {
        $biodatas = Biodata::all();

        //Cek kondisi data ada atau tidak
        if (count($biodatas) > 0) {
            $res['message'] = "Success";
            $res['data'] = $biodatas;
        } else {
            $res["message"] = "No Data";
            $res['data'] = [];
        }

        return response()->json($res);
    }
    public function add(Request $request)
    {
        //Create new Object dari property $request
        $biodata = new Biodata(
            ['first_name' => $request->first_name, 'last_name' => $request->last_name]
        );

        if ($biodata->save()) {
            $res['message'] = "Success";
            $res['data'] = $biodata;
        } else {
            $res["message"] = "Biodata gagal disimpan!";
            $res['data'] = [];
        }

        return response()->json($res);
    }

    public function edit($biodataId)
    {
        $biodata = Biodata::find($biodataId);
        return response()->json($biodata);
    }

    public function update($biodataId, Request $request)
    {
        $biodata = Biodata::find($biodataId);
        $biodata->first_name = $request->first_name;
        $biodata->last_name = $request->last_name;

        if ($biodata->save()) {
            $res['message'] = "Success";
            $res['data'] = $biodata;
        } else {
            $res["message"] = "Biodata gagal diupdate!";
            $res['data'] = [];
        }

        return response()->json($res);
    }

    public function delete($biodataId)
    {
        $biodata = Biodata::find($biodataId);

        if ($biodata->delete()) {
            $res['message'] = "Biodata berhasil dihapus.";
        } else {
            $res["message"] = "Biodata gagal dihapus.";
        }

        return response()->json($res);
    }
}
