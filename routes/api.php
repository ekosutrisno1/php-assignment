<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/all', 'Api\BiodataController@index');

Route::group(['prefix' => 'biodata'], function () {
    Route::post('add', 'Api\BiodataController@add');
    Route::get('edit/{id}', 'Api\BiodataController@edit');
    Route::post('update/{id}', 'Api\BiodataController@update');
    Route::delete('delete/{id}', 'Api\BiodataController@delete');
});
