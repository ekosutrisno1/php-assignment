# PHP ASSIGMENT API

Setup dan Install

port : <http://localhost:8000>, frontend ada pada folder resource/js

- Create database di php My admin dengan nama *php_assignment*

- Pastikan sudah install composer

```bash
composer install
```

```bash
php artisan migrate
```

- Install Vue

```bash
npm install
```

- Serve Laravel App

```bash
php artisan serve
```
