<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1">

   <title>Laravel</title>

   <!-- Fonts -->
   <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

   <!-- Styles -->
   <style>
      html,
      body {
         background-color: #fff;
         font-family: 'Nunito', sans-serif;
         font-weight: 200;
         height: 100vh;
         margin: 0;
      }

      .full-height {
         height: 100vh;
      }

      .flex-center {
         align-items: center;
         display: flex;
         justify-content: center;
      }

      .position-ref {
         position: relative;
      }

      .top-right {
         position: absolute;
         right: 10px;
         top: 18px;
      }

      .content {
         text-align: center;
      }

      .title {
         font-size: 84px;
      }

      .m-b-md {
         margin-bottom: 30px;
      }

      .button--green {
         display: inline-block;
         border-radius: 4px;
         border: 1px solid #3b8070;
         color: #3b8070;
         text-decoration: none;
         padding: 12px 30px;
         font-weight: bold
      }

      .button--green:hover {
         color: #fff;
         background-color: #3b8070;
      }
   </style>
</head>

<body>
   <div class="flex-center position-ref full-height">
      @if (Route::has('login'))
      <div class="top-right links">
         @auth
         <a href="{{ url('/home') }}">Home</a>
         @else
         <a href="{{ route('login') }}">Login</a>

         @if (Route::has('register'))
         <a href="{{ route('register') }}">Register</a>
         @endif
         @endauth
      </div>
      @endif

      <div class="content">
         <div class="title m-b-md">
            PHP ASSIGNMENT
         </div>

         <div class="links">
            <a class="button--green" href="/home">Goto Dashboard</a>
         </div>
      </div>
   </div>
</body>

</html>
