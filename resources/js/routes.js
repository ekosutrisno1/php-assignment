import Home from './pages/Home';
import AddBiodata from './pages/AddBiodata';
import EditBiodata from './pages/EditBiodata';

export const routes = [
   {
      name: 'home',
      path: '/home',
      component: Home
   },
   {
      name: 'add',
      path: '/add',
      component: AddBiodata
   },
   {
      name: 'edit',
      path: '/edit/:id',
      component: EditBiodata
   }
];
